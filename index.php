<?php
require_once 'goutte.phar';
use Goutte\Client;

# - 毎週のバッチの開始時に実行
# php index.php start_crawle
# - 商品データの取得
# php index.php crawle_detail
# 
#print text_format("          TEST");

$db_file= "db/db2.sqlite3";
$version    = 1; //XXX
$wait_time  = 1; // 秒
$begin_page = 1; // XXX
//$max        = 10;//一回当たりの取得ページ数

main($argv);
function main($argv){
  $db = get_db();

  //global $version;
  //print "version : {$version}";
  $client = new Client();

  $mode = $argv[1];
  if($mode == 'start_crawle'){
    # 一覧ページから、タイトル,URLを取得しDBに格納する
    # 
    get_product_list($client);
  }
  elseif($mode == 'crawle_list'){
    print "B";
    # select
    # crawle
    # INSERT INTO product , or update
    # update crawle_version
  }
  elseif($mode == 'crawle_detail'){
    # 商品詳細データの取得
    update_product_data($client);
  }
  else{
    print "else $mode";
  }
}

function update_product_data($client){
  print "[START] crawle_detail";
  # select * FROM product WHERE crawle_version < $version AND deleted =0 ORDER BY id LIMIT 10;
  # crawle
  $detail_url = "http://japan.webike.net/products/1540654.html"; #通常

  #$detail_url = "http://japan.webike.net/products/1540755.html";#こちらがサイズ別ありの商品です。
  #$detail_url = "http://japan.webike.net/products/9366695.html";#こちらが色別ありの商品です。
  $data = get_detail($client,$detail_url);
  # save imageXXX
  # update product , or update

}

function get_product_list($client){
  print "START CRAWLE";

  // INSERT Page
  /*
    $stmt = $db->prepare("INSERT INTO crawle_version (query,max_page) VALUES(:query,:max_page)");
    $stmt->bindValue(':query'   , "limit200", SQLITE3_TEXT);
    $stmt->bindValue(':max_page', 1393      , SQLITE3_TEXT);
    $result = $stmt->execute();
  */
  $max_page = get_max_page($client);
  ##max page調査、
  #insert
  $page = 1; //XXX
  
  $max_page = 557;
  $max_per_page= 500;
  for($p = 495; $p <= $max_page ; $p++){
    //print " get_list( $client,$max_per_page,$page)\n";

    $list = get_list($client,$max_per_page,$p);
    print_r($list);
    for($i = 0; $i < count($list); ++$i) {
      $data = $list[$i];
      insert_or_update_product( $db,$data["title"],$data["href"] ,$i);
    }
    update_version($db,1,$p);//XXXversion
    sleep(25);
  }
}

function get_db(){
  global $db_file;
  try {
    $db = new SQLite3($db_file);
  } catch (Exception $e) {
    print 'DBへの接続でエラーが発生しました。<br>';
    print $e->getTraceAsString();
  }
  return $db;
}

function tf($str){
  return text_format($str);
}
function text_format($str){
  $str = preg_replace('/^\s+/u', '', $str);
  $str = preg_replace('/\n\s+/u', '\n', $str);
  return $str;
}

function update_version($db,$id,$did_page){
  $stmt = $db->prepare("UPDATE crawle_version SET did_page = :did_page WHERE id =:id");
  $stmt->bindValue(':id'       , $id , SQLITE3_TEXT);
  $stmt->bindValue(':did_page' , $did_page, SQLITE3_TEXT);
  $result = $stmt->execute();
}

function parse_product_code($href){
  preg_match("|http://japan.webike.net/products/(\d*).html|is",$href,$match);
  return $match[1];
}

function insert_or_update_product( $db,$title,$href ,$cnt){
  print "insert or update : $title,$href \n";
  $product_code  = parse_product_code($href);
  print "$product_code \n";
  $stmt = $db->prepare("INSERT INTO product (product_code,name,url,crawle_version)
  VALUES (:product_code,:name,:url,:crawle_version)");
  $stmt->bindValue(':product_code'  , $product_code , SQLITE3_INTEGER);//XXX
  $stmt->bindValue(':name'          , $title, SQLITE3_TEXT);
  $stmt->bindValue(':url'           , $href, SQLITE3_TEXT);
  $stmt->bindValue(':crawle_version', 1    , SQLITE3_TEXT);
  $result = $stmt->execute();
}

function get_detail($client,$url){
  #・名前（必須）
  #・値段（望ましい）
  #・車種（必須）
  #・商品説明（望ましい）
  # 画像 #最低１枚
  # 色
  # サイズ

  $crawler = $client->request('GET',$url);
  $data =array();
  print "access to ${url}";

  $data['title']      = tf($crawler->filter('.prod_title')->text() );#名前
  $data['price_doll'] = tf($crawler->filter('.price-value-current')->text() );#値段(doll)
  $data['price_yen']  = tf($crawler->filter('.price-value-yen')->text() );#値段(円)
  $data['fits']       = tf($crawler->filter('.fitsB')->text() );#車種
  $descs = $crawler->filter('.descBody')->each(function ($node){
      return $node->text();
    });
  #$data['desc']       = tf( $crawler->filter('.descBody')->text() ) ;#車種
  $data['desc'] = tf( join("\n", $descs ) ) ;
  # 画像取得処理
  # XXXX
  #http://img.webike.net/catalogue/images/8888/031579_01.jpg

  #$images = $crawler->filter('li a')->each(function ($node){
  $images = $crawler->filter('img.product_img')->each(function ($node){
      print "XXX";
      return $node->attr('src');
    });
  print "IMAGES";
  print_r($images);
  exit();
  
  $selects = $crawler->filter('.optionVals')->each(function ($node){
      return $node->text();
    });
  # color
  #selectedOptions
  # size
  print_r($data);
  print_r($selects);
}

function get_max_page($client){
  $crawler = $client->request('GET','http://japan.webike.net/bm/pl/Custom+Parts/1000/');
  // 抽出
  $max_page = 0;
  $targetSelector = '.displayinfo span'; // soukensuu
  $crawler->filter(".searchParam2")->each(function ($node) {
      $tmp = $node->text();
      global $max_page;
      if (preg_match("/^[0-9]+$/", $tmp)) {
	if($tmp > $max_page ){
	  $max_page = $tmp;
	}
      }
    });
  global $max_page;
  return $max_page;
}

function get_list($client,$limit,$page){
#http://japan.webike.net/bm/pl/Custom+Parts/1000/#!search&p.rows=100&p.ref=categories&p.c=1000
  #$crawler = $client->request('GET','http://japan.webike.net/bm/pl/Custom+Parts/1000/');
#  $crawler = $client->request('GET','http://japan.webike.net/bm/pl/Custom+Parts/1000/#!search&p.rows=100&p.ref=categories&p.c=1000');
  print "http://japan.webike.net/bm/pl/Custom+Parts/1000/?p.rows={$limit}&p.p={$page}";

  $crawler = $client->request('GET',"http://japan.webike.net/bm/pl/Custom+Parts/1000/?p.rows={$limit}&p.p={$page}");
  $lists = $crawler->filter(".product-name a")->each(function ($node) {
      echo $node->text() . ",";
      echo $node->attr('href') . "\n";
      return array( 'title'=> $node->text() , 'href' => $node->attr('href') );
    });
#print_r($lists);
  return $lists;
}
function test($client){
  // 2http://www.time-j.net/から「東京の過去36時間の天気」を取得
  $crawler = $client->request('GET',
			      'http://www.time-j.net/WorldTime/Weather/Weather36h/Asia/Tokyo');
  
  // 3「東京の過去36時間の天気」テーブルを指定
  $dom = $crawler->filter('table.wtable td');
  
  $ary = array(); // 「現地時間」、「天気」の保存用
  $time = "";     // 「現地時間」の一時保管用
  $ix = 0;        // 現在行

  // 4テーブルから1行ずつ取得する
  $dom->each(function ($node) use (&$ix, &$time, &$ary) {
      
      // 5「現地時間」を取得する
      if (($ix % 8)==0) {
	$time = $node->text();
      }
      // 6「天気」を取得する
      else if ((($ix-1) % 8)==0) {
	$ary[ $time ] = $node->text();
      }
      $ix++;
    });
  
  // 7現地時間、天気を表示する
  foreach ($ary as $t => $w){
    echo $t. " ". $w. "<br />";
  }
};
