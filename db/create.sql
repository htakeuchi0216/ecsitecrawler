create table product(
       id integer primary key autoincrement,
       product_code integer unique,
       name text,
       url  text,
       crawle_version integer not null ,
       deleted  integer not null default 0,
       created_at default CURRENT_TIMESTAMP,
       updated_at default CURRENT_TIMESTAMP
);

create table crawle_version(
       id integer primary key autoincrement,
       query text ,
       did_page integer not null default 0,
       max_page integer not null,
       finished integer default 0,
       created_at default CURRENT_TIMESTAMP
);

INSERT INTO  crawle_version (id,query,max_page) VALUES(1,"limit500",557);